Breve descrizione del contenuto degli scripts:

- FaceServerDiscovery:
Tramite una chimata HTTP di tipo Get (attraverso la UnityWebRequest), questa classe scarica un file di indici e date, elabora quest'ultimi creando gli appositi oggetti che saranno usati come parametri di una callaback, il tutto racchiuso in un Task ascincrono

- MultiplayerLobbyHandler
è il gestore della Lobby di un gioco racing multiplayer. Ho inserito questa classe per mostrare l'utilizzo del pattern Observer, delle propietà ([ClientRpc] [Server] [Command]) della libreria Mirror e della gestione e manipolazione di bit

- SavingDataHandler
è un esempio (che mi porto dietro nei progetti) per salvare i dati in locale

-ScenesManager
è un esempio (che mi porto dietro nei progetti) per gestire ordinatamente il caricamento e scaricamento delle scene