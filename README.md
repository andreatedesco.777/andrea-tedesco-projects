# Andrea Tedesco Projects

- [**5x10**](https://play.google.com/apps/testing/com.Games4u.fivebyten) (Mobile Game)
Team composition: 1 Programmer, 2 3DArtists.
  - Made with Unity and published on PlayStore.
  - Includes interactions with GooglePlay APIs (LeaderBoard and IAP) and Unity Ads.
  - Integrated sharing and asynchronous loading of objects for the shop based on indexes obtained through client-server calls.

</br>

- **HARRIERS** (Multiplayer Game)
Team composition: 4 Programmers, 4 Designers, 3 Concept Artists, 3 3DArtists.
Harriers is a pc and console racing game exclusively focused on multiplayer mode, in which the player
competes in illegal races, in which contestants use futuristic wingsuits (Falcon, Marvel Cinecomix)
named Harriers through the roofs of a post-modern New York. Players will compete in a series of maps
full of alternative routes and obstacles, in which even weather conditions will play a significant role, as
the wingsuits will be engineered to take advantage of some of these.
  - Made with Unity.
  - As Lead Pogrammer, I contributed to development of the project by interfacing with the respective department Leads and
supervising the other Programmers in the development of the code.
  - I personally took care of the Multiplayer component, the Game Play Mechanics, the Saving/Loading game data System, the Management of the Player and the Managers Scripts of the game. 
Choosing to use the Observer Pattern as a method of interfacing with scripts,
facilitated the supervision of my team members and the development of a more linear code.

</br>

- [**Pushami**](https://play.google.com/store/apps/details?id=com.first.pushami) (Mobile Game)
Team composition: 1 Programmer.
Challenge your friends, beat your opponents online or break your personal best.
  - Made with Unity and published on PlayStore. 
  - Includes interactions with GooglePlay APIs (LeaderBoard, Multiplayer and IAP) and Unity Ads.

</br>

- **Road to Zakkarias** (Global Game Jam 2020)
Team composition: 4 Programmers, 1 Designers, 3 Concept Artists.
A story about an elderly robot musician that wants to rock again with his old band. 
So he decides to find and repair his companions to play together again at the Zakkarias festival.

</br>

- **The Cathedral**
Team composition: 4 Programmers, 4 Designers, 3 Concept Artists, 3 3DArtists. 
Guide Gordon to discover the eighth wonder of the world.  
  - Made with Unity.
  - As Lead Pogrammer, I contributed to development of the project by interfacing with the respective department Leads and
supervising the other Programmers in the development of the code.
  - I personally took care of the Game Play Mechanics, the Saving/Loading game data System, the Management of the Player and the Managers Scripts of the game. 

</br>

- [**Xenosaurs**](https://play.google.com/apps/internaltest/4701685237879681091) (Mobile Game)
Team composition: 4 Programmers, 5 Designers, 3 Concept Artists, 3 3DArtists.
  - Made with Unity  and published on PlayStore as Internal Test.
  - Includes interactions with GooglePlay APIs (LeaderBoard and IAP).
  - Incledes Unity Ads.